var sliderint = 1;

var slidernext = 2;

$(document).ready(function(){
	$("#slider > img#1").fadeIn(300);
	$("#Previous").click(prevSlider);
	$("#Next").click(nextSlider);
	startSlider();
});

function showSlide(slide){
	stopLoop();
	count = $("#slider > img").size();

	if(slide > count){
		slide = 1;
		sliderint = 1;
	}else if (slide < 1){
		slide = count;
	}
	$("#slider > img").fadeOut(300);
	$("#slider > img#" + slide).fadeIn(300);

	sliderint= slide;
	slidernext = slide + 1;
	startSlider();
}

function startSlider(){
	count = $("#slider > img").size();

	loop = setInterval(function(){
		if(slidernext > count){
			slidernext = 1;
			sliderint = 1;
		}

		$("#slider > img").fadeOut(300);
		$("#slider > img#" + slidernext).fadeIn(300);

		sliderint= slidernext;
		slidernext = slidernext + 1;
	},3000)
}

function nextSlider(){
	slidernext = sliderint + 1;
	showSlide(slidernext);

}

function prevSlider(){
	slidernext = sliderint - 1;
	showSlide(slidernext);
}

function stopLoop() {
	window.clearInterval(loop);
}

$("#slider > img").hover(
	function(){
		stopLoop();
	},
	function(){
		startSlider();
	}
);