function newGrid(){
	var gridSize = prompt("what gridsize?");
	var penColor = "black";

	for (var i=0; i<gridSize; i++){
		for(var j=0; j< gridSize; j++){
			$("#container").append("<div id="+i+j+" class="+'square'+"></div>");
			$("#"+i+j+"").width(960/gridSize);
			$("#"+i+j+"").height(960/gridSize);
		}
	}

	$(".square").hover(function(){
		$(this).css('background-color', penColor);
	});
};

newGrid();

$("#clearGrid").click(function(){
	$("#container").empty();
	newGrid();
})

$("#red").click(function(){
	penColor = "red";
	$(".square").hover(function(){
		$(this).css('background-color', penColor);
	});
});

$("#green").click(function(){
	penColor = "green";
	$(".square").hover(function(){
		$(this).css('background-color', penColor);
	});
});

$("#blue").click(function(){
	penColor = "blue";
	$(".square").hover(function(){
		$(this).css('background-color', penColor);
	});
});

$("#black").click(function(){
	penColor = "black";
	$(".square").hover(function(){
		$(this).css('background-color', penColor);
	});
});